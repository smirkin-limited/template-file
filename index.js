const pify = require('pify');
const readFile = pify(require('fs').readFile);

function renderString(template, data) {
  return template.replace(/\{\{\s?([\s\S]*?)\s?\}\}/g, (match, captured) => {
    let result;
    eval(`with (data) { result = ${captured} }`);

    if (typeof result === 'function') {
      result = result();
    }

    return result;
  });
}

function renderTemplateFile(filepath, data) {
  return readFile(filepath, 'utf8').then(templateString =>
    renderString(templateString, data)
  );
}

module.exports = {
  renderString,
  renderTemplateFile
};
